﻿var catTableRange = tableLoss.getRange();
var catRangeForChart = catTableRange.getColumn(0).getResizedRange(0, 2);
var catChart = outputSheet.charts.add(Excel.ChartType.pie,
    catRangeForChart,
    Excel.ChartSeriesBy.columns
);
catChart.title.text = "Profits by Category";
catChart.legend.load();
catChart.legend.overlay = true;
catChart.legend.visible = true;
catChart.legend.position = "Corner";
catChart.style = 4;

var catChartStart = catTableRange.getLastRow().getOffsetRange(-2, 3);
catChart.setPosition(catChartStart, catChartStart.getOffsetRange(10, 10));