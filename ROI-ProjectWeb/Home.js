﻿(function () {
    "use strict";

    var cellToHighlight;
    var messageBanner;

    // The initialize function must be run each time a new page is loaded.
    Office.initialize = function (reason) {
        $(document).ready(function () {

            // Initialize the FabricUI notification mechanism and hide it
            var element = document.querySelector('.ms-MessageBanner');
            messageBanner = new fabric.MessageBanner(element);
            messageBanner.hideBanner();

            $('#financial').click(financialAnalysis);
            $('#runAll').click(runAll);

        });
    };

    function runAll() {
        financialAnalysis();
    }
    function financialAnalysis() {




        var RawDataTableName = "ROI";
        var OutputSheetName = "Financial Analysis";

        var today = new Date();
        var alreadyExists = false;

        Excel.run(function (context) {

            //Retreive today's date for use below
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = mm + '-' + dd + '-' + yyyy;

            OutputSheetName = "" + today + " Financial Analysis";
            var originalTable = context.workbook.tables.getItem(RawDataTableName);

            




            //Retreive the columns needed for financial analysis
            var nameCol = originalTable.columns.getItem("Name");         
            var pricePaidCol = originalTable.columns.getItem("Price Paid");
            var priceSoldCol = originalTable.columns.getItem("Price Sold");
            var categoryCol = originalTable.columns.getItem("Category");

            nameCol.load("values");          
            pricePaidCol.load("values");
            priceSoldCol.load("values");           
            categoryCol.load("values");

            var sheets = context.workbook.worksheets;
            sheets.load("items/name");

            return context.sync().then(function () {

                for (var x = 0; x < sheets.items.length; x++) {
                    if (sheets.items[x].name == OutputSheetName) {
                        alreadyExists = true;
                    }
                }

                if (alreadyExists) {
                    showNotification("Whoops...", "Sheet " + OutputSheetName + " already exists. Please remove it if you want to update today's analysis.");
                }

                var data = [];
                var totalProfits = 0;

                //loop through retrieved records and only add them if the sale price is not zero (0 indicates item has not been sold)
                for (var i = 1; i < nameCol.values.length; i++) {

                    var name = nameCol.values[i][0];
                    var category = categoryCol.values[i][0];
                    var pricePaid = pricePaidCol.values[i][0];
                    var priceSold = priceSoldCol.values[i][0];
                    var itemProfit = priceSold - pricePaid;
          
                    if (priceSold != 0) {
                        data.push({ name: name, pricePaid: pricePaid, priceSold: priceSold, itemProfit: itemProfit, category: category });
                        totalProfits += itemProfit;
                    }
                }

                //find and display the profits of each categories based on name
                var profitByCategory = []; //will hold each unique category
                var tempArray = []; //will hold the names of categories already added
                for (var i = 0; i < data.length; i++) {
                    if (tempArray.indexOf(data[i].category) == -1) { //if this categorie has not yet been added, add it
                        tempArray.push(data[i].category);
                        profitByCategory.push({ category: data[i].category, cumulativeProfit: (data[i].itemProfit), count: 1 });
                    }
                    else {
                        profitByCategory[tempArray.indexOf(data[i].category)].cumulativeProfit += (data[i].itemProfit);
                        profitByCategory[tempArray.indexOf(data[i].category)].count++;
                    }
                }

                //Sort top 10 biggest profits
                var sorted = data.sort(function (item1, item2) {
                    return item2.itemProfit - item1.itemProfit;
                });
                var top10 = sorted.slice(0, 10);

                var outputSheet = context.workbook.worksheets.add(OutputSheetName);

                /*
                 * Table and chart for top 10 best profits
                 */
                var totalProfitCell = outputSheet.getRange("B1");
                var returnCell = "Total return on investment:";
                totalProfitCell.values = [[returnCell]];
                totalProfitCell.format.font.bold = true;
                totalProfitCell.format.font.size = 14;
                var totalProfitVal = outputSheet.getRange("C1");
                totalProfitVal.values = [[totalProfits]];
                totalProfitVal.numberFormat = "$#.00";
                totalProfitVal.format.font.size = 14;

                var sheetHeaderTitle = "Top 10 Largest Profits";
                var tableCategories = ["Name", "Price Paid", "Profit", "Price Sold", "Category"];

                var reportStartCell = outputSheet.getRange("B3");
                reportStartCell.values = [[sheetHeaderTitle]];
                reportStartCell.format.font.bold = true;
                reportStartCell.format.font.size = 14;

                var table = outputSheet.tables.add('B4:F4', true /*has headers*/);
                table.name = "ProfitTable";
                table.getHeaderRowRange().values = [tableCategories];

                for (var i = 0; i < top10.length; i++) {
                    var itemData = top10[i];
                    table.rows.add(null, [[
                        itemData.name,
                        itemData.pricePaid,                      
                        itemData.itemProfit,
                        itemData.priceSold,
                        itemData.category
                    ]]);
                }

                table.getDataBodyRange().getColumn(1).numberFormat = "$#.00";
                table.getDataBodyRange().getColumn(2).numberFormat = "$#.00";
                table.getDataBodyRange().getColumn(3).numberFormat = "$#.00";
             
                var fullTableRange = table.getRange();
                var dataRangeForChart = fullTableRange.getColumn(0).getResizedRange(0, 2);               
                var chart = outputSheet.charts.add(Excel.ChartType.columnStacked,
                    dataRangeForChart,
                    Excel.ChartSeriesBy.columns
                );
                chart.title.text = "Top 10 Largest Profits";
                chart.legend.load();
                chart.legend.overlay = true;
                chart.legend.visible = true;
                chart.legend.position = "Corner";
               
           
                var chartPositionStart = fullTableRange.getLastRow().getOffsetRange(-10, 6);
                chart.setPosition(chartPositionStart, chartPositionStart.getOffsetRange(10, 6));

                //////////////////////////////////////////
                 /*
                 * Table and chart for top 10 worst profits
                 */

                var sortedDown = data.sort(function (item1, item2) {
                    return item1.itemProfit - item2.itemProfit;
                });
                top10 = sortedDown.slice(0, 10);
          
                var sheetHeaderTitleDown = "Top 10 Worst Returns";         
                var reportStartCellDown = outputSheet.getRange("B16");
                reportStartCellDown.values = [[sheetHeaderTitleDown]];
                reportStartCellDown.format.font.bold = true;
                reportStartCellDown.format.font.size = 14;

                var tableLoss = outputSheet.tables.add('B17:F17', true /*has headers*/);
                tableLoss.name = "LossTable";
                tableLoss.getHeaderRowRange().values = [tableCategories];

                for (var i = 0; i < top10.length; i++) {
                    var itemData = top10[i];
                    tableLoss.rows.add(null, [[
                        itemData.name,
                        itemData.pricePaid,
                        itemData.itemProfit,
                        itemData.priceSold,                     
                        itemData.category
                    ]]);
                }

                //Currency formatting
                tableLoss.getDataBodyRange().getColumn(1).numberFormat = "$#.00";
                tableLoss.getDataBodyRange().getColumn(2).numberFormat = "$#.00";
                tableLoss.getDataBodyRange().getColumn(3).numberFormat = "$#.00";

                var lossTableRange = tableLoss.getRange();
                var lossRangeForChart = lossTableRange.getColumn(0).getResizedRange(0, 2);
                var lossChart = outputSheet.charts.add(Excel.ChartType.columnStacked,
                    lossRangeForChart,
                    Excel.ChartSeriesBy.columns
                );
                lossChart.title.text = "Top 10 Smallest Returns";
                //Enable the legend and put it in the top right corner
                lossChart.legend.load();
                lossChart.legend.overlay = true;
                lossChart.legend.visible = true;
                lossChart.legend.position = "Corner";
                

                var lossChartStart = lossTableRange.getLastRow().getOffsetRange(-10, 6);
                lossChart.setPosition(lossChartStart, lossChartStart.getOffsetRange(10, 6));


               //////////////////////////////////////////
                 /*
                 * Table and chart for profits by category
                 */

                var headerCategory = "Profit By Category";
                var headerPosCategory = outputSheet.getRange("B29");
                headerPosCategory.values = [[headerCategory]];
                headerPosCategory.format.font.bold = true;
                headerPosCategory.format.font.size = 14;

                var categoryCategories = ["Category","Profit"];
                var tableCategory = outputSheet.tables.add('B30:C30', true /*has headers*/);
                tableCategory.name = "CategoryTable";
                tableCategory.getHeaderRowRange().values = [categoryCategories];

                for (var i = 0; i < profitByCategory.length; i++) {
                    var itemData = profitByCategory[i];
                    tableCategory.rows.add(null, [[
                        itemData.category,
                        itemData.cumulativeProfit
                    ]]);
                }

                var catTableRange = tableCategory.getRange();
                var catRangeForChart = catTableRange.getColumn(0).getResizedRange(0, 1);
                var catChart = outputSheet.charts.add(Excel.ChartType.pie,
                    catRangeForChart,
                    Excel.ChartSeriesBy.columns
                );
                catChart.title.text = "Profits by Category";
                catChart.legend.load();
                catChart.legend.overlay = true;
                catChart.legend.visible = true;
                catChart.legend.position = "Corner";
                

                var catChartStart = catTableRange.getLastRow().getOffsetRange(-2, 3);
                catChart.setPosition(catChartStart, catChartStart.getOffsetRange(10, 4));

                ////////////////////////////////////////

                if (Office.context.requirements.isSetSupported("ExcelAPI", 1.2)) {              
                    //Autofit column widths
                    table.getRange().getEntireColumn().format.autofitColumns();

                    chart.style = 8;
                    lossChart.style = 4;
                    catChart.style = 10;
                }

                outputSheet.activate();

            }).then(context.sync);
        }).catch(function (error) {
            console.log(error);
            //Log additional stuff if you need to
            if (error instanceof OfficeExtension.Error) {
                console.log(error.debugInfo);
            }
        })
    }

    function hightlightHighestValue() {
        // Run a batch operation against the Excel object model
        Excel.run(function (ctx) {
            // Create a proxy object for the selected range and load its properties
            var sourceRange = ctx.workbook.getSelectedRange().load("values, rowCount, columnCount");

            // Run the queued-up command, and return a promise to indicate task completion
            return ctx.sync()
                .then(function () {
                    var highestRow = 0;
                    var highestCol = 0;
                    var highestValue = sourceRange.values[0][0];

                    // Find the cell to highlight
                    for (var i = 0; i < sourceRange.rowCount; i++) {
                        for (var j = 0; j < sourceRange.columnCount; j++) {
                            if (!isNaN(sourceRange.values[i][j]) && sourceRange.values[i][j] > highestValue) {
                                highestRow = i;
                                highestCol = j;
                                highestValue = sourceRange.values[i][j];
                            }
                        }
                    }

                    cellToHighlight = sourceRange.getCell(highestRow, highestCol);
                    sourceRange.worksheet.getUsedRange().format.fill.clear();
                    sourceRange.worksheet.getUsedRange().format.font.bold = false;

                    // Highlight the cell
                    cellToHighlight.format.fill.color = "orange";
                    cellToHighlight.format.font.bold = true;
                })
                .then(ctx.sync);
        })
        .catch(errorHandler);
    }

    function displaySelectedCells() {
        Office.context.document.getSelectedDataAsync(Office.CoercionType.Text,
            function (result) {
                if (result.status === Office.AsyncResultStatus.Succeeded) {
                    showNotification('The selected text is:', '"' + result.value + '"');
                } else {
                    showNotification('Error', result.error.message);
                }
            });
    }

    // Helper function for treating errors
    function errorHandler(error) {
        // Always be sure to catch any accumulated errors that bubble up from the Excel.run execution
        showNotification("Error", error);
        console.log("Error: " + error);
        if (error instanceof OfficeExtension.Error) {
            console.log("Debug info: " + JSON.stringify(error.debugInfo));
        }
    }

    // Helper function for displaying notifications
    function showNotification(header, content) {
        $("#notification-header").text(header);
        $("#notification-body").text(content);
        messageBanner.showBanner();
        messageBanner.toggleExpansion();
    }
})();
